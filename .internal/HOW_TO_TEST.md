# How add unit testing with gtest to a project:
## Clone + CMake (e.g. on Windows)
- Clone https://github.com/google/googletest.git or add it as a submodule to your project
- In CMake add googletest as a subdirectory:

	> `add_subdirectory(googletest)`

- Add a new executible with example/test/test_main.cpp and link it against gtest and gmock

	> `add_executable(unit_test test_main.cpp)`

	> `target_link_libraries(unit_test PUBLIC gtest gmock)`


## Package manager + Any build system (e.g. on Linux)
- Install packages libgtest-dev libgmock-dev
- In your build system add a new test executable and link it agains gtest and gmock (-lgtest -gmock)
- You can copy the main file from example/test/test_main.cpp or just add linkage with -lgmock_main

# Reference:
https://google.github.io/googletest

# Cheat sheet:
https://qiangbo-workspace.oss-cn-shanghai.aliyuncs.com/2018-12-05-gtest-and-coverage/PlainGoogleQuickTestReferenceGuide1.pdf