#!/bin/sh
set -e

# Go to script dir
cd "$(dirname $(readlink -f $0))"

# Clean
rm -rf out
mkdir -p out

# Source files
SRC="node.cpp text_node.cpp animation.cpp animation_manager.cpp renderer.cpp moving_boxes.cpp utf8_str.cpp"

# Build app
cd src
g++ -o ../out/app -I../include $SRC main.cpp -lncursesw

# Build test
g++ -c -I../include --coverage $SRC ../test/test_main.cpp
g++ -o ../out/test -coverage *.o -lncursesw -lgmock -lgtest
rm *.o
