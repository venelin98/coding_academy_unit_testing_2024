#pragma once

class Animation
{
public:
	Animation() = default;
	Animation(const Animation&);
	Animation(Animation&&);
	Animation& operator=(const Animation&);
	Animation& operator=(Animation&&);
	~Animation();

	// Start animation from begin
	void start();
	// Start animation from where it left off
	void resume();
	// Stop the animation, it can be resumed later
	void stop();
	// Jump to the given position between begin and end
	void set_position(float);

	// called by animation_manager each frame
	void update_state(unsigned ms_since_last);

	bool is_running() const;
	bool is_reversed() const;

	struct
	{
		float begin = 0;
		float end = 0;
		unsigned duration = 0;
		unsigned executions = 1;      // 2 execution = 1 cycle (begin -> end -> begin)
	}cfg;
	int* prop;

private:
	unsigned elapsed_ms_;	// since animation start
	unsigned elapsed_executions_;
	bool running_ = false;
	bool reversed_;
};
