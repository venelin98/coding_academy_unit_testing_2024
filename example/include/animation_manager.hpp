#pragma once

#include "animation.hpp"

namespace animation_manager
{
	void registerAnimation(Animation*);
	void unregisterAnimation(Animation*);
	void processAnimations(unsigned ms_since_last);
};
