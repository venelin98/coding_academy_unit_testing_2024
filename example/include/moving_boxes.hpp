#pragma once
#include <vector>
#include "animation.hpp"
#include "node.hpp"

class MovingBoxes
{
public:
	explicit MovingBoxes(unsigned top_y);
	void add_box(Color=BLACK);
	void move_to(unsigned top_y);
private:
	std::vector<Node*> boxes_;
	std::vector<Animation> anims_;
	unsigned top_y_;
};
