#pragma once
#include "renderer.hpp"

class Node
{
public:
	virtual ~Node() = default;

	virtual void draw() const;

	int x=0, y=0;
	unsigned width=0, height=0;
	Color color = BLACK;
};
