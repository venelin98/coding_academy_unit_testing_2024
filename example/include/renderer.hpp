#pragma once

enum Color
{
	BLACK,
	RED,
	YELLOW,
	GREEN,
	BLUE,
	MAGENTA,
	CYAN,
	WHITE,
};

class Node;
class TextNode;

namespace renderer
{
	void init();
	void render_frame();

	int screen_width();
	int screen_height();

	template <class NodeT>
	NodeT* create_node();
}
