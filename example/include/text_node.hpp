#pragma once
#include "node.hpp"
#include "utf8_str.hpp"

class TextNode: public Node
{
public:
	void draw() const override;

	utf8_str text;
};

class TextBoxNode: public TextNode
{
public:
	void draw() const override;
};
