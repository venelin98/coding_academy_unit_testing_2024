#pragma once
#include <string>

// UTF-8 string where indexes refer to symbols rather then bytes
class utf8_str
{
public:
	utf8_str() = default;

	template <class Str>
	utf8_str(Str&& s) : str_(std::forward<Str>(s)) {}

	// Implicit cast to string
	operator std::string&();
	operator std::string() const;

	utf8_str& insert(size_t index, const utf8_str& to_insert);
	utf8_str& erase(size_t index, size_t count=1);

	// Symbol index to byte index - returns the byte index of the start of the symbol
	// Returns std::string::npos if the sym_i is too large
	size_t sym_to_byte(size_t sym_i) const;
	// Lenght in symbols
	size_t len() const;
	// Size in bytes
	size_t bytes() const;
	// Number of lines
	size_t lines() const;
	// Access n-th symbol
	std::string operator[](size_t n) const;
	// Handle to internal string
	const std::string& str() const;

	// Create a substring consisting of 'count' symbols, starting from 'start'
	utf8_str substr(size_t count) const;
	utf8_str substr(size_t start, size_t count) const;

	// String wrappers
	template <class T>
	utf8_str& operator+=(T&& rhs) {
		str_ += std::forward<T>(rhs);
		return *this;
	}
	template <class T>
	utf8_str& operator=(T&& rhs) {
		str_ = std::forward<T>(rhs);
		return *this;
	}

private:
	std::string str_;
};
