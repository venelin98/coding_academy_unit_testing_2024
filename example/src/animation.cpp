#include <string.h>
#include "animation.hpp"
#include "animation_manager.hpp"

Animation::Animation(const Animation& oth)
{
	memcpy(this, &oth, sizeof(Animation));
	if( oth.is_running() )
		animation_manager::registerAnimation(this);
}

Animation::Animation(Animation&& oth)
{
	memcpy(this, &oth, sizeof(Animation));
	if( oth.is_running() )
		animation_manager::registerAnimation(this);
}

Animation& Animation::operator=(const Animation& oth)
{
	if(this != &oth)
	{
		memcpy(this, &oth, sizeof(Animation));
		if( oth.is_running() )
			animation_manager::registerAnimation(this);
	}
	return *this;
}

Animation& Animation::operator=(Animation&& oth)
{
	if(this != &oth)
	{
		memcpy(this, &oth, sizeof(Animation));
		if( oth.is_running() )
			animation_manager::registerAnimation(this);
	}
	return *this;
}

Animation::~Animation()
{
	animation_manager::unregisterAnimation(this);
}

void Animation::start()
{
	elapsed_ms_ = 0;
	elapsed_executions_ = 0;
	running_ = true;
	reversed_ = false;
	animation_manager::registerAnimation(this);
}

void Animation::stop()
{
	running_ = false;
	animation_manager::unregisterAnimation(this);
}

void Animation::set_position(float pos)
{
	if(reversed_)
		elapsed_ms_ = (pos - cfg.end) / (cfg.begin - cfg.end) * cfg.duration;
	else
		elapsed_ms_ = (pos - cfg.begin) / (cfg.end - cfg.begin) * cfg.duration;
}

bool Animation::is_running() const
{
	return running_;
}

bool Animation::is_reversed() const
{
	return reversed_;
}

void Animation::update_state(unsigned ms_since_last)
{
	elapsed_ms_ += ms_since_last;

	float new_val;

	if(elapsed_ms_ > cfg.duration)
	{
		++elapsed_executions_;

		if(elapsed_executions_ >= cfg.executions)
		{
			running_ = false;
		}
		else
		{
			reversed_ = !reversed_;
			elapsed_ms_ = 0;
		}

		new_val = reversed_ ? cfg.begin : cfg.end;
	}

	if(reversed_)
		new_val = cfg.end - (cfg.end - cfg.begin) * (static_cast<float>(elapsed_ms_) / cfg.duration);
	else
		new_val = cfg.begin + (cfg.end - cfg.begin) * (static_cast<float>(elapsed_ms_) / cfg.duration);

	*prop = new_val;
}
