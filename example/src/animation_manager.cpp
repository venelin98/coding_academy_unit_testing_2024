#include <algorithm>
#include <vector>
#include "animation_manager.hpp"

namespace animation_manager
{

    using AnimIt = std::vector<Animation*>::iterator;

    static std::vector<Animation*> anim_collection_;

    void registerAnimation(Animation* anim)
    {
        auto beginIt = anim_collection_.begin();
        auto endIt = anim_collection_.end();
        if (std::find(beginIt, endIt, anim) == endIt)
        {
            anim_collection_.push_back(anim);
        }
    }

    void unregisterAnimation(Animation* anim)
    {
        AnimIt end = anim_collection_.end();
        for(AnimIt it = anim_collection_.begin(); it != end; ++it)
        {
            if(*it == anim)
            {
                *it = anim_collection_.back();
                anim_collection_.pop_back();
                break;
            }
        }
    }

    void processAnimations(unsigned ms_since_last)
    {
        int last = static_cast<int>(anim_collection_.size() - 1);
        for(int i=0; i<=last;)   /* Iterate through all animations, if any is finished remove it */
        {
            anim_collection_[i]->update_state(ms_since_last);
            if(!anim_collection_[i]->is_running())
            {
                anim_collection_[i] = anim_collection_[last];
                anim_collection_.pop_back();
                --last;
            }
            else
            {
                ++i;
            }
        }
    }
}
