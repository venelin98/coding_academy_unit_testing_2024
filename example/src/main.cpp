#include <chrono>
#include <thread>
#include "text_node.hpp"
#include "animation_manager.hpp"
#include "moving_boxes.hpp"

using namespace std::chrono_literals;
using namespace std::chrono;

int main(int argc, char* argv[])
{
	renderer::init();

	// Frame
	Node* frame = renderer::create_node<Node>();
	frame->width = renderer::screen_width();
	frame->height = renderer::screen_height();

	// Welcome text
	TextNode* welcome = renderer::create_node<TextNode>();
	welcome->width = 75;
	welcome->height = 15;
	welcome->x = 22;
	welcome->y = 10;
	welcome->text =
		"__        __   _                          _ \n"
		"\\ \\      / /__| | ___ ___  _ __ ___   ___| |\n"
		" \\ \\ /\\ / / _ \\ |/ __/ _ \\| '_ ` _ \\ / _ \\ |\n"
		"  \\ V  V /  __/ | (_| (_) | | | | | |  __/_|\n"
		"   \\_/\\_/ \\___|_|\\___\\___/|_| |_| |_|\\___(_)\n"
		"                                            \n";
	Animation clr;
	clr.cfg.end = BLACK;
	clr.cfg.end = WHITE;
	clr.cfg.duration = 10000;
	clr.cfg.executions = 99999;
	clr.prop = (int*)&welcome->color;
	clr.start();

	// Desctipton
	TextNode* desc = renderer::create_node<TextNode>();
	desc->width = 30;
	desc->height = 4;
	desc->x = 32;
	desc->y = 17;
	desc->color = CYAN;
	desc->text = "To a unit testing lecture by:";

	// Author
	TextBoxNode* txt = renderer::create_node<TextBoxNode>();
	txt->width = 9;
	txt->height = 4;
	txt->x = 41;
	txt->y = 19;
	txt->color = BLUE;
	txt->text = "Венелин";

	// Moving Boxes
	MovingBoxes moving_boxes(7);
	moving_boxes.add_box(WHITE);
	moving_boxes.add_box(WHITE);
	moving_boxes.add_box(GREEN);
	moving_boxes.add_box(GREEN);
	moving_boxes.add_box(RED);
	moving_boxes.add_box(RED);


	while(true)
	{
		animation_manager::processAnimations(30);

		renderer::render_frame();

		std::this_thread::sleep_for(30ms);

		frame->width = renderer::screen_width();
		frame->height = renderer::screen_height();
	}

	return 0;
}
