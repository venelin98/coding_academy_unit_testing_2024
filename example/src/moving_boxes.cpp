#include "moving_boxes.hpp"

MovingBoxes::MovingBoxes(unsigned top_y)
	: top_y_(top_y)
{
}

void MovingBoxes::add_box(Color color)
{

	Node* box = renderer::create_node<Node>();
	box->width = 4;
	box->height = 4;
	box->color = color;

	unsigned new_x;
	if(!boxes_.empty())
	{
		box->y = boxes_.back()->y + 4;

		new_x = boxes_.back()->x + boxes_.size()*4;
	}
	else	// First box
	{
		box->y = top_y_;
		new_x = 0;
	}

	anims_.emplace_back();
	Animation& anim = anims_.back();
	anim.cfg.end = renderer::screen_width() - 4;
	anim.cfg.duration = 7500;
	anim.cfg.executions = 99999;
	anim.prop = &box->x;	// todo

	anim.start();
	anim.set_position(new_x);

	boxes_.push_back(box);
}

void MovingBoxes::move_to(unsigned top_y)
{
	for(unsigned i=0; i < boxes_.size(); ++i)
	{
		boxes_[i]->y = top_y + i*4;
	}
}
