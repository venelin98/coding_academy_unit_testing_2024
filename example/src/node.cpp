#include <ncursesw/ncurses.h>
#include "node.hpp"

void Node::draw() const
{
	WINDOW* pad = newpad(height, width);

	wbkgd(pad, COLOR_PAIR(color));
	box(pad, 0, 0);

	const int pbegY = y>0 ? 0 : -y;		  /* pad top left coords to be displayed */
	const int pbegX = x>0 ? 0 : -x;
	pnoutrefresh(pad,
		     pbegY,     pbegX,
		     y,     x,
		     LINES - 1, COLS - 1);

	delwin(pad);
}
