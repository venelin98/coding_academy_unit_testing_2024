#include <vector>
#include <locale.h>
#include <ncursesw/ncurses.h>
#include <stdlib.h>
#include "renderer.hpp"
#include "node.hpp"
#include "text_node.hpp"

namespace renderer
{
	static std::vector<Node*> nodes_;

	void init()
	{
		setlocale(LC_ALL, ""); // ncurses use UTF-8

		/* Init ncurses */
		initscr();			                /* start curses mode */
		atexit( (void(*)())endwin );		/* stop curses at exit */
		curs_set(0);		                /* Hide cursor */
		noecho();
		cbreak();	                     /* get input instantly */
		nodelay(stdscr, true);              // make getch non-blocking
		keypad(stdscr, true);
		start_color();
		use_default_colors();
		/* bkgd(COLOR_PAIR(5)); */
		init_pair(CYAN, COLOR_CYAN, -1); /* -1 means use default */
		init_pair(BLACK, COLOR_BLACK, -1);
		init_pair(RED, COLOR_RED, -1);
		init_pair(GREEN, COLOR_GREEN, -1);
		init_pair(YELLOW, COLOR_YELLOW, -1);
		init_pair(BLUE, COLOR_BLUE, -1);
		init_pair(MAGENTA, COLOR_MAGENTA, -1);
		init_pair(WHITE, COLOR_WHITE, -1);

		wnoutrefresh(stdscr);					/* if not present the first real refresh is missed */

	}

	void render_frame()
	{
		erase(); //Clear the screen
		wnoutrefresh(stdscr);

		for(const Node* n: nodes_)
			n->draw();

		doupdate();	// Update screen
	}

	int screen_width()
	{
		return COLS;
	}

	int screen_height()
	{
		return LINES;
	}

	template <class NodeT>
	NodeT* create_node(){
		auto n = new NodeT;
		nodes_.push_back(n);
		return n;
	}

	template Node*        create_node<Node>();
	template TextNode*    create_node<TextNode>();
	template TextBoxNode* create_node<TextBoxNode>();
}
