#include <ncursesw/ncurses.h>
#include "text_node.hpp"

void TextNode::draw() const
{
	WINDOW* pad = newpad(height, width);

	wbkgd(pad, COLOR_PAIR(color));
	mvwaddstr(pad, 0, 0, text.str().data());

	const int pbegY = y>0 ? 0 : -y;		  /* pad top left coords to be displayed */
	const int pbegX = x>0 ? 0 : -x;
	pnoutrefresh(pad,
		     pbegY,     pbegX,
		     y,         x,
		     LINES - 1, COLS - 1);

	delwin(pad);
}

void TextBoxNode::draw() const
{
	WINDOW* pad = newpad(height, width);

	wbkgd(pad, COLOR_PAIR(color));
	box(pad, 0, 0);

	if(height > 2 and width > 2)
	{
		utf8_str display = text.substr(width - 2);

		mvwaddstr(pad, height / 2, 1, display.str().data());
	}

	const int pbegY = y>0 ? 0 : -y;		  /* pad top left coords to be displayed */
	const int pbegX = x>0 ? 0 : -x;
	pnoutrefresh(pad,
		     pbegY,     pbegX,
		     y,         x,
		     LINES - 1, COLS - 1);

	delwin(pad);
}
