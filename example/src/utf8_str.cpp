#include "utf8_str.hpp"

// Is c the start of a UTF-8 symbol
static bool is_sym(char c)
{
	return (c & 0xC0) != 0x80;
}

utf8_str::operator std::string&()
{
	return str_;
}

utf8_str::operator std::string() const
{
	return str_;
}

utf8_str& utf8_str::insert(size_t index, const utf8_str& to_insert)
{
	size_t byte_i = sym_to_byte(index);
	if( byte_i != std::string::npos )
		str_.insert(byte_i, to_insert.str_);
	return *this;
}

utf8_str& utf8_str::erase(size_t index, size_t count)
{
	size_t start_byte = sym_to_byte(index);
	if (start_byte == std::string::npos)
		return *this; // Index out of range, nothing to erase

	size_t end_byte = start_byte; // end_byte is past-the-end index
	size_t sym_count = 0;
	while (end_byte < str_.size())
	{
		if ( is_sym(str_[end_byte]) )
		{
			++sym_count;
			if(sym_count > count)
				break;
		}
		++end_byte;
	}

	str_.erase(start_byte, end_byte - start_byte);
	return *this;
}

size_t utf8_str::sym_to_byte(size_t sym_i) const
{
	size_t syms = 0;
	size_t byte_i = 0;
	for (; byte_i < str_.size(); ++byte_i)
	{
		if (is_sym(str_[byte_i]))
		{
			if (syms == sym_i)
				return byte_i;
			++syms;
		}
	}

	return std::string::npos;
}

size_t utf8_str::len() const
{
	size_t num = 0;
	for(char c: str_)
	{
		if( is_sym(c) )
			++num;
	}

	return num;
}

size_t utf8_str::bytes() const
{
	return str_.size();
}

size_t utf8_str::lines() const
{
	if(str_.empty())
		return 0;

	size_t num = 1;
	for(char c: str_)
	{
		if(c == '\n')
			++num;
	}

	return num;
}

std::string utf8_str::operator[](size_t n) const
{
	return substr(n, 1);
}

const std::string& utf8_str::str() const
{
	return str_;
}

utf8_str utf8_str::substr (size_t count) const
{
	return substr(0, count);
}

utf8_str utf8_str::substr(size_t start, size_t count) const
{
	size_t byte_i = sym_to_byte(start);
	if ( byte_i == std::string::npos)
		return utf8_str(); // Return an empty utf8_str if start index is out of range

	size_t syms = start;
	utf8_str result;
	for(size_t byte_i = 0; byte_i < str_.size(); ++byte_i)
	{
		if( is_sym(str_[byte_i]) )
			++syms;

		if(syms > start + count)
			break;

		else if(syms > start)
			result += str_[byte_i];
	}

	return result;
}
