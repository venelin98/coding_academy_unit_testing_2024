#include <gmock/gmock.h>
#include "utf8_str.hpp"

TEST(UTF8_STR, len)
{
	utf8_str latin("ABC");

	EXPECT_EQ(latin.len(), 3);

	utf8_str cyr("АБВ");

	EXPECT_EQ(cyr.len(), 3);
}

TEST(UTF8_STR, insert)
{
	utf8_str test_string("ABC");

	test_string.insert(1, "XYZ");
	EXPECT_EQ(test_string.str(), "AXYZBC");

	utf8_str cyr_string("АБВ");
	cyr_string.insert(2, "Ч");
	EXPECT_EQ(cyr_string.str(), "АБЧВ");
}

int main(int argc, char* argv[])
{
	testing::InitGoogleMock();
	return RUN_ALL_TESTS();
}
